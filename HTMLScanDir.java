/*
	Basically, take the path of a directory, get a list of all files in the directory
	and output that list to an html document
*/
import java.util.Scanner;
import java.util.ArrayList;
import java.util.Arrays;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.BufferedWriter;
import java.nio.file.FileSystemException;

public class HTMLScanDir{
	private static final String HEADER_START = "<html>\n\t<body>\n\t\t<h1>Contents of ";
	private static final String HEADER_END = "\t\t</ul>\n\t</body>\n</html>";

	private static void writeDirectoryFilesToHTML(File directory) throws IOException{
		ArrayList<String> contents = new ArrayList<String>(Arrays.asList(directory.list()));

		File output = new File("output.html");
		try(BufferedWriter writer = new BufferedWriter(new FileWriter(output))){
			writer.write(HEADER_START + directory.getCanonicalPath() + ":</h1>\n\t\t<ul>\n\t\t\t");

			int content_size = contents.size();
			for(int counter = 0; counter < content_size; counter++){
				System.out.println(contents.get(counter));
				writer.write("\t\t\t<li> " + contents.get(counter) + " </li>\n");
			}

			writer.write(HEADER_END);
		}
	}
	public static void main(String[] args){
		try(Scanner userInput = new Scanner(System.in)){
			File testInput;
			do{
				System.out.print("Please enter the name of the directory: ");
				String input = userInput.next();
				testInput = new File(input);
			}
			while(!testInput.isDirectory());
			writeDirectoryFilesToHTML(testInput);
		}
		catch(IOException io_exc){
			System.out.println("An error occurred: " + io_exc.getMessage());
		}
	}
}
