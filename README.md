![image](https://gitlab.com/chenshuiluke/HTMLScanDir/raw/master/screenshot.png)

A simple java program that will scan a directory and output its contents to a HTML file.

Requirements:
===

1. Java
2. Meson
3. Ninja

Building:
===

	[luke@luke-server HTMLScanDir]$ mkdir build
	[luke@luke-server HTMLScanDir]$ meson build

	[luke@luke-server HTMLScanDir]$ cd build
	[luke@luke-server build]$ ninja
	[2/2] Creating jar HTMLScanDir.jar.


